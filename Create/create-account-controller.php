<?php

require '../Account-Entities.php';

class CreateAccController extends Account {
    public function __construct($name, $usrnm, $pswd, $phone, $profile) {
        parent::__construct($name, $usrnm, $pswd, $phone, $profile);
    }

    //ANY EXTRA VALIDATION, JUST ADD
    public function validateDetails() {
        $boo;

        if(empty($this->name) || empty($this->usrnm) || empty($this->pswd) || empty($this->phone)){
            $boo = "emptyInput";
        }

        else {
            $booC ="";
            $booUS; 
            $booPH;
            $array = array('VU'=>parent::verifyUsername() , 'VP'=>parent::verifyPhone());
            
            foreach($array as $booName=>$booValue) {
                switch($booName) {
                    case 'VU': 
                        if($booValue == false)
                            $booUS = "UE";
                        else
                            $booUS = "";
                        $booC .= $booUS;
                        break;

                        case 'VP': 
                        if ($booValue == false) 
                            $booPH = "PE";
                        else   
                            $booPH = "";
                        $booC .= $booPH;
                        break;
                }
            }

            if($booC == "UE")
                $boo = "UExist";
            else if($booC == "PE")
                $boo = "PExist";
            else if($booC == "UEPE")
                $boo = "UPExist";
            else   
                $boo = "CreateValid";
        } 
        return $boo; 
    }

    public function addUser() {
        parent::addUser();
    }
}